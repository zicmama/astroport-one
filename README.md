# Astroport ONE

Jeu d'aventure écrit en bash qui permet d'activer une ambassade IPFS Astroport.
- Inscription de visiteurs "Extraterrestres"
- Contrat réciproque terraformation Foret Jardin
- VISA Passeport Terre - Formation Astronaute
- Définition des Talents . JEu

# Préambule
Quand pourrons nous sortir de ce Méchant Cloud qui nous profile, nous scrute, nous analyse... Pour au final nous faire consommer.
Je n'ai pas suivi la formation d’ingénieur réseau pour fabriquer ça!
Alors j'ai fait autre chose.

Astroport est basé sur IPFS pour former nos Cloud personnels échangés entre amis d'amis à l’abri des algorithmes de l'IA et du datamining sauvage qui règne ici bas.
L'avantage de ce système, une consommation énergétique divisé par 100, une répartition des données qui permet de fonctionner déconnecté, un système d'information pair à pair inaltérable, inviolable.
S'il vous plaît arrêtons cet Internet Supermarché de nous même...
C'est une bibliothèque du savoir à la base.

https://astroport.com
Avec cette technologie, nous devenons chacun hébergeur et fournisseur d'accès, souverain monétaire et médiatique.
Avec cette technologie, nous établissons une "Crypto Nation" dont la carte relie les territoires au delà des frontières et des pays.
Astroport ONE est une ambassade.

# Astroport.ONE

Il s'agit d'un Jeu de société grandeur nature qui consiste à répertorier, inventer, enseigner, diffuser les meilleures façons d'habiter la planète Terre.
Ce programme introduit des données multimédia (page web, audio, vidéo) en tant que chaines de données (blockchain) inscrites dans le réseau IPFS
que les joueurs échangent au travers des Oasis.

Astroport One c'est aussi une performance scientifique et artistique néé le 25 oct 2021.
Dans une forêt de 8ha située en France, dans le Tarn et Garonne, une groupe de bénéficiaires du RSA
s'est réuni, pour jouer le rôle de membres d'une "NASA Extraterrestes" installés en forêt qu'ils transforment en jardin.

Cette exploration met en oeuvre l'usage de "Monnaie Libre" appliquée à ce JEu de société.
Chaque nouveau joueur reçoit 300 LOVE (correspondant à 3 DU(G1) afin dévaluer sa capacité à pratiquer des activités ayant une valeur pour le commun.

Chacun est invité à enregistrer ses idées et propositions pour améliorer la qualité de vie du lieu.
Ces publications sont enregistrées comme REVES du lieu.

Chaque joueur aura pour mission, de répartir le montant de sa production monétaire journalière, 100 LOVE entre les ACTIONS et les REVES déclarés.
Il devra également s'acquiter du montant établi pour assurer sa pension complête, montant réduit au 1/3 quand le joueur aura offert un nouvel habitat.

Les règles évoluent encore... Pour rejoindre l'expérience :

Astroport 20h12
https://carte.monnaie-libre.fr/?id=22023



---

Bienvenue dans la confédération intergalactique.

Ambassade des Astronautes Terraformeurs.

Avant de commencer l'aventure, découvrez cet ouvrage "The Barefoot Architect" de Johan Van Lengen.
```
ipfs ls Qme6a6RscGHTg4e1XsRrpRoNbfA6yojC6XNCBrS8nPSEox/
ipfs get -o /tmp/vdoc.pub_the-barefoot-architect.pdf QmbfVUAyX6hsxTMAZY7MhvUmB3AkfLS7KqWihjGfu327yG
```
Il fait partie de la base documentaire officielle des Astronautes Terraformeurs.
Chacun transporte son mediaCenter personnel et collectif qui se propage entre amis d'amis (niveau de confiance relatifs à l'exercice d'un Talent) au travers de station "Ambassade IPFS Astroport"

Ces Stations IPFS localisés à des cooordonnées précises délimitent et conserve l'historique des rêves et réalisations que les astronautes et leurs talents mettent en oeuvre.

Dans IPFS, chacun publie sous forme de blockchain des contenus multimedia dont l'index est publié comme site web statique et "rembobinable" comportant texte, image, sons et vidéos (selon les modèles situés dans Templates)


DELARATION DES LIEUX
~/.zen/game/worlds

- Coord GPS (Map Minetest)
- Certification des niveaux d'aptitudes à utiliser des outils ou équipements.
- Niveaux autonomies:  eau, nourriture, couchages, electricité, chaleur, conservation, ...
- Ambassade & Enregistrement des joueurs ("VISA") : Passeport Terre.
- Collecte & publication des rêves modèle Pinterest à venir, rempli lors du visionnage des rêves et plans de tous.


NAVIGATEUR JOUEUR
~/.zen/game/players

- Identité astronaute + VISA
- talents / équipements
- arbres blockchain informationnels, projection des rêves, évaluation des talents,

---

# TODO
* Ajouter des worlists au choix par oasis https://diceware.readthedocs.io/en/stable/wordlists.html
* Remplacer le chiffrage SSL par PGP
* Traductions
